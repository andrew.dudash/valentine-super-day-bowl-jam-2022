import logging

from engine import Engine


def main():
    # logging.getLogger().setLevel(logging.INFO)
    logging.getLogger().setLevel(logging.DEBUG)
    engine = Engine()
    engine.run()


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print()
        pass
