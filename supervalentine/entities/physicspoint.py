import math
import logging

DELTA_MULTIPLIER = 0.01
MOTION_THRESHOLD = 0.1
DEFAULT_X_DRAG_COEF = 0.75
DEFAULT_Y_DRAG_COEF = 0.75
DEFAULT_DRAG_EXPONENT = 1.0
DEFAULT_GRAVITY_ACCEL = 50


class PhysicsPoint():
    """PhysicsPoint has no awareness of the game itself, but performs x and y axis physics calculations."""

    def __init__(self):
        self.x_accel = 0.0
        self.y_accel = 0.0
        self.x_drag_coef = 0.0
        self.y_drag_coef = 0.0
        self.gravity_accel = 0.0
        self.x_velocity_min = 0.0
        self.y_velocity_min = 0.0
        self.x_velocity_max = 0.0
        self.y_velocity_max = 0.0
        self.x_velocity = 0.0
        self.y_velocity = 0.0
        self.x_movement = 0.0
        self.y_movement = 0.0

    def set_accel(self, x, y):
        self.x_accel = float(x)
        self.y_accel = float(y)

    def set_accel_x(self, x):
        self.x_accel = float(x)

    def set_accel_y(self, y):
        self.y_accel = float(y)

    def stop_accel(self):
        self.x_accel = 0.0
        self.y_accel = 0.0

    def set_velocity_x_range(self, x_min, x_max):
        self.x_velocity_min = float(x_min)
        self.x_velocity_max = float(x_max)

    def set_velocity_y_range(self, y_min, y_max):
        self.y_velocity_min = float(y_min)
        self.y_velocity_max = float(y_max)

    def set_velocity(self, x, y):
        self.x_velocity = float(x)
        self.y_velocity = float(y)

    def set_velocity_x(self, x):
        self.x_velocity = float(x)

    def set_velocity_y(self, y):
        self.y_velocity = float(y)

    def bump_velocity(self, x, y):
        self.x_velocity += float(x)
        self.y_velocity += float(y)

    def stop(self):
        self.x_accel = 0.0
        self.y_accel = 0.0
        self.x_drag_coef = 0.0
        self.y_drag_coef = 0.0
        self.gravity_accel = 0.0
        self.x_velocity = 0.0
        self.y_velocity = 0.0
        self.x_movement = 0.0
        self.y_movement = 0.0

    def enable_drag(self, x_drag_coef=DEFAULT_X_DRAG_COEF, y_drag_coef=DEFAULT_Y_DRAG_COEF):
        self.x_drag_coef = x_drag_coef
        self.y_drag_coef = y_drag_coef

    def disable_drag(self):
        self.x_drag_coef = 0.0
        self.y_drag_coef = 0.0

    def enable_gravity(self, gravity_accel=DEFAULT_GRAVITY_ACCEL):
        self.gravity_accel = gravity_accel

    def disable_gravity(self):
        self.gravity_accel = 0.0

    def is_moving(self):
        return math.sqrt(math.pow(self.x_velocity, 2.0) + math.pow(self.y_velocity, 2.0)) > MOTION_THRESHOLD

    def tick(self, dt):
        # do the math for 1 cycle (tick)
        # the fewer hz, the longer each tick will be
        dt *= DELTA_MULTIPLIER
        logging.debug(f'PhysicsPoint dt: {dt}')
        logging.debug(
            f'PhysicsPoint starting velocity: ({self.x_velocity}, {self.y_velocity})')

        # standard acceleration'
        logging.debug(
            f'PhysicsPoint accel: ({self.x_accel}, {self.y_accel})')
        self.x_velocity += self.x_accel * dt
        self.y_velocity += self.y_accel * dt

        # gravity
        logging.debug(
            f'PhysicsPoint accel gravity: {self.gravity_accel}')
        self.y_velocity += self.gravity_accel * dt

        # drag
        logging.debug(
            f'PhysicsPoint drag coef: ({self.x_drag_coef}, {self.y_drag_coef})')
        drag_accel_x = 0.0
        drag_accel_y = 0.0
        if self.is_moving():
            drag_accel_x = math.pow(
                self.x_velocity, DEFAULT_DRAG_EXPONENT) * self.x_drag_coef
            drag_accel_y = math.pow(
                self.y_velocity, DEFAULT_DRAG_EXPONENT) * self.y_drag_coef
        logging.debug(
            f'PhysicsPoint drag accel: ({drag_accel_x}, {drag_accel_y})')
        self.x_velocity -= drag_accel_x * dt
        self.y_velocity -= drag_accel_y * dt

        # velocity limits
        logging.debug(
            f'PhysicsPoint x velocity range: ({self.x_velocity_min}, {self.x_velocity_max})')
        logging.debug(
            f'PhysicsPoint y velocity range: ({self.y_velocity_min}, {self.y_velocity_max})')
        self.x_velocity = max(self.x_velocity, self.x_velocity_min)
        self.x_velocity = min(self.x_velocity, self.x_velocity_max)
        self.y_velocity = max(self.y_velocity, self.y_velocity_min)
        self.y_velocity = min(self.y_velocity, self.y_velocity_max)

        # final movement calculation
        logging.debug(
            f'PhysicsPoint velocity: ({self.x_velocity}, {self.y_velocity})')
        if self.is_moving():
            self.x_movement += self.x_velocity * dt
            self.y_movement += self.y_velocity * dt

    def integer_movement(self):
        # subtract and return the integer (pixel) component of movement
        x_movement_int = math.floor(self.x_movement)
        y_movement_int = math.floor(self.y_movement)
        self.x_movement -= x_movement_int
        self.y_movement -= y_movement_int
        logging.debug(
            f'PhysicsPoint tick integer movement: ({x_movement_int}, {y_movement_int})')
        return (x_movement_int, y_movement_int)
