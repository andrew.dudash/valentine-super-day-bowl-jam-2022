import pygame

BG_MOVE_SPEED_MULTIPLIER = 0.025


class Background:

    def __init__(self):
        self.bgimage = pygame.image.load(
            "supervalentine/assets/background.png")
        self.rectBGimage = self.bgimage.get_rect()

        self.bg_x1 = 0
        self.bg_y1 = 0

        self.bg_x2 = self.rectBGimage.width
        self.bg_y2 = 0

        self.bg_move_speed = 0

    def update(self, char_pos):
        if char_pos < 320:
            self.bg_move_speed = char_pos - 320
        elif char_pos > 960:
            self.bg_move_speed = char_pos - 960
        else:
            self.bg_move_speed = 0

        self.bg_x1 -= self.bg_move_speed * BG_MOVE_SPEED_MULTIPLIER
        self.bg_x2 -= self.bg_move_speed * BG_MOVE_SPEED_MULTIPLIER

        if self.bg_x1 <= -self.rectBGimage.width:
            self.bg_x1 = self.rectBGimage.width
        if self.bg_x2 <= -self.rectBGimage.width:
            self.bg_x2 = self.rectBGimage.width

    def render(self, screen):
        screen.blit(self.bgimage, (self.bg_x1, self.bg_y1))
        screen.blit(self.bgimage, (self.bg_x2, self.bg_y2))
