import logging
import pygame
import itertools

from enum import Enum, auto

import entities.physicspoint as physicspoint
from entities.physicspoint import PhysicsPoint

ANIMATION_INTERVAL = 100


class PlayerState(Enum):
    IDLE = auto()
    WALKING_L = auto()
    WALKING_R = auto()
    JUMPING = auto()
    FALLING = auto()
    CROUCHING = auto()


class Player(pygame.sprite.Sprite):

    def __init__(self, init_midbottom_x, init_midbottom_y):
        super(Player, self).__init__()
        self.player_state = PlayerState.IDLE
        self.physics_point = PhysicsPoint()
        self.tick_movement = (0, 0)
        self.tick_counter = 0
        self.facing_left = False

        # import walking images
        self.walking_images_raw = [
            pygame.image.load(
                'supervalentine/assets/valentine_walk_64x64_1.png'),
            pygame.image.load(
                'supervalentine/assets/valentine_walk_64x64_2.png'),
            pygame.image.load(
                'supervalentine/assets/valentine_walk_64x64_3.png')
        ]
        self.walking_right_images = [pygame.transform.scale(
            x, (128, 128)) for x in self.walking_images_raw]
        self.walking_left_images = [pygame.transform.flip(
            x, 1, 0) for x in self.walking_right_images]
        self.walking_right_animation = itertools.cycle(
            self.walking_right_images)
        self.walking_left_animation = itertools.cycle(self.walking_left_images)

        # import jumping images
        self.jumping_images_raw = [
            pygame.image.load(
                'supervalentine/assets/valentine_jump_64x64_1.png'),
            pygame.image.load(
                'supervalentine/assets/valentine_jump_64x64_2.png'),
        ]
        self.jumping_right_images = [pygame.transform.scale(
            x, (128, 128)) for x in self.jumping_images_raw]
        self.jumping_left_images = [pygame.transform.flip(
            x, 1, 0) for x in self.jumping_right_images]
        self.jumping_right_animation = itertools.cycle(
            self.jumping_right_images)
        self.jumping_left_animation = itertools.cycle(self.jumping_left_images)

        # import falling images
        self.falling_images_raw = [
            pygame.image.load(
                'supervalentine/assets/valentine_fall_64x64_1.png'),
            pygame.image.load(
                'supervalentine/assets/valentine_fall_64x64_2.png'),
        ]
        self.falling_right_images = [pygame.transform.scale(
            x, (128, 128)) for x in self.falling_images_raw]
        self.falling_left_images = [pygame.transform.flip(
            x, 1, 0) for x in self.falling_right_images]
        self.falling_right_animation = itertools.cycle(
            self.falling_right_images)
        self.falling_left_animation = itertools.cycle(self.falling_left_images)

        self.surf = self.walking_right_images[0]
        self.rect = self.surf.get_rect()
        self.rect.move_ip(init_midbottom_x - self.rect.width / 2, init_midbottom_y - self.rect.height)

    def change_state(self, state):
        self.player_state = state
        self.tick_counter = ANIMATION_INTERVAL

    def cmd_idle(self):
        if self.player_state == PlayerState.WALKING_L or \
           self.player_state == PlayerState.WALKING_R or \
           self.player_state == PlayerState.CROUCHING:
            # start idling
            self.change_state(PlayerState.IDLE)
            self.physics_point.set_velocity_x_range(-100, 100)
            self.physics_point.set_velocity_y_range(-100, 100)
            self.physics_point.stop_accel()
            self.physics_point.enable_drag(2, 1)
            self.physics_point.disable_gravity()

    def cmd_walk_left(self):
        if self.player_state == PlayerState.IDLE or \
           self.player_state == PlayerState.WALKING_R:
            # start walking left
            self.change_state(PlayerState.WALKING_L)
            self.physics_point.set_velocity_x_range(-100, -25)
            self.physics_point.set_velocity_y_range(0, 0)
            self.physics_point.set_accel(-70, 0)
            self.physics_point.enable_drag()
            self.physics_point.disable_gravity()

    def cmd_walk_right(self):
        if self.player_state == PlayerState.IDLE or \
           self.player_state == PlayerState.WALKING_L:
            # start walking right
            self.change_state(PlayerState.WALKING_R)
            self.physics_point.set_velocity_x_range(25, 100)
            self.physics_point.set_velocity_y_range(0, 0)
            self.physics_point.set_accel(70, 0)
            self.physics_point.enable_drag()
            self.physics_point.disable_gravity()

    def cmd_jump(self):
        if self.player_state == PlayerState.IDLE or \
           self.player_state == PlayerState.WALKING_L or \
           self.player_state == PlayerState.WALKING_R:
            # start jumping
            self.change_state(PlayerState.JUMPING)
            self.physics_point.set_velocity_x_range(-100, 100)
            self.physics_point.set_velocity_y_range(-200, 200)
            self.physics_point.bump_velocity(0, -150)
            self.physics_point.stop_accel()
            self.physics_point.enable_drag(0.025, 0.5)
            self.physics_point.enable_gravity()

    def cmd_crouch(self):
        if self.player_state == PlayerState.IDLE or \
           self.player_state == PlayerState.WALKING_L or \
           self.player_state == PlayerState.WALKING_R:
            # start crouching
            self.change_state(PlayerState.CROUCHING)
            self.physics_point.set_velocity_x_range(-100, 100)
            self.physics_point.set_velocity_y_range(0, 0)
            self.physics_point.stop_accel()
            self.physics_point.enable_drag(2, 2)
            self.physics_point.disable_gravity()

    def update(self, dt):
        logging.debug(f'Player state: {self.player_state}')
        logging.debug(f'Player tick counter: {self.tick_counter}')
        self.physics_point.tick(dt)
        self.tick_movement = self.physics_point.integer_movement()
        self.rect.move_ip(self.tick_movement)
        self.tick_counter += dt

        # check jumping/falling state
        if self.player_state == PlayerState.JUMPING:
            if self.physics_point.y_velocity > 0:
                self.change_state(PlayerState.FALLING)

        # determine left/right face
        if self.physics_point.is_moving():
            if self.physics_point.x_velocity < 0:
                self.facing_left = True
            else:
                self.facing_left = False

        # handle animations
        if self.tick_counter > ANIMATION_INTERVAL:
            self.tick_counter -= ANIMATION_INTERVAL
            if self.player_state == PlayerState.IDLE:
                if self.facing_left:
                    self.surf = self.walking_left_images[0]
                else:
                    self.surf = self.walking_right_images[0]
            elif self.player_state == PlayerState.WALKING_L:
                self.surf = next(self.walking_left_animation)
            elif self.player_state == PlayerState.WALKING_R:
                self.surf = next(self.walking_right_animation)
            elif self.player_state == PlayerState.JUMPING:
                if self.facing_left:
                    self.surf = next(self.jumping_left_animation)
                else:
                    self.surf = next(self.jumping_right_animation)
            elif self.player_state == PlayerState.FALLING:
                if self.facing_left:
                    self.surf = next(self.falling_left_animation)
                else:
                    self.surf = next(self.falling_right_animation)
            elif self.player_state == PlayerState.CROUCHING:
                pass

    def bounce(self, bounce_x, bounce_y):
        logging.debug(f'Player bounce: ({bounce_x}, {bounce_y})')
        if abs(bounce_x) > 0:
            self.physics_point.set_accel_x(0)
            self.physics_point.set_velocity_x(0)
            self.rect.move_ip((bounce_x, 0))
        if abs(bounce_y) > 0:
            self.physics_point.set_accel_y(0)
            self.physics_point.set_velocity_y(0)
            self.rect.move_ip((0, bounce_y))

        # check for ground collision
        if bounce_y < 0:
            if self.player_state == PlayerState.FALLING:
                self.change_state(PlayerState.IDLE)
                self.physics_point.set_velocity_x_range(-100, 100)
                self.physics_point.set_velocity_y_range(0, 0)
                self.physics_point.stop_accel()
                self.physics_point.enable_drag(2, 2)
                self.physics_point.disable_gravity()

    def render(self, screen):
        logging.debug(f'Player rect: {self.rect}')
        screen.blit(self.surf, self.rect)
