import logging
import pygame

from enum import Enum, auto

from entities.physicspoint import PhysicsPoint


class TownieState(Enum):
    IDLE = auto()


class Townie(pygame.sprite.Sprite):

    def __init__(self, init_midbottom_x, init_midbottom_y):
        super(Townie, self).__init__()
        self.townie_state = TownieState.IDLE
        self.physics_point = PhysicsPoint()
        self.tick_movement = (0, 0)
        self.tick_counter = 0
        self.facing_left = False

        self.surf = pygame.Surface((128, 128))
        self.surf.fill((128, 128, 128))
        self.rect = self.surf.get_rect()
        self.rect.move_ip(init_midbottom_x - self.rect.width / 2, init_midbottom_y - self.rect.height)

    def update(self, dt):
        logging.debug(f'Townie state: {self.townie_state}')
        logging.debug(f'Townie tick counter: {self.tick_counter}')
        self.physics_point.tick(dt)
        self.tick_movement = self.physics_point.integer_movement()
        self.rect.move_ip(self.tick_movement)
        self.tick_counter += dt

    def bounce(self, bounce_x, bounce_y):
        pass

    def render(self, screen):
        logging.debug(f'Townie rect: {self.rect}')
        screen.blit(self.surf, self.rect)
