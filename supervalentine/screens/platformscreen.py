import logging
import pygame

from entities.background import Background
from entities.player import Player
from entities.townie import Townie


class KeyManager:
    """Keep track of key presses as a stack, not just as up/down events."""

    def __init__(self):
        self.key_dict = {
            'ESCAPE': False,
            'W': False,
            'A': False,
            'S': False,
            'D': False,
            'J': False,
            'K': False,
            'SPACE': False,
        }
        self.key_stack = []

    def key_down(self, key):
        if key in self.key_dict:
            if self.key_dict[key]:
                self.key_stack.remove(key)
            self.key_stack.append(key)
            self.key_dict[key] = True

    def key_up(self, key):
        if key in self.key_dict:
            if self.key_dict[key]:
                self.key_stack.remove(key)
                self.key_dict[key] = False

    def last_key_down(self):
        if len(self.key_stack) == 0:
            return None
        return self.key_stack[-1]


class PlatformScreen:

    def __init__(self, engine, width, height):
        self.engine = engine
        self.width = width
        self.height = height
        self.key_manager = KeyManager()
        self.midcenter_x = self.width / 2
        self.ground_level = self.height - 64

        # init background
        self.bg = Background()

        # init player
        self.player = Player(self.midcenter_x, self.ground_level)

        # init townies
        self.townies = []
        self.townies.append(Townie(self.midcenter_x * 0.5, self.ground_level))
        self.townies.append(Townie(self.midcenter_x * 1.5, self.ground_level))

        # init annotations
        self.instruction_font = pygame.font.Font(None, 24)
        self.instruction_color = (32, 32, 32)
        self.instruction_text = self.instruction_font.render(
            'Level 1 DEMO',
            1,
            self.instruction_color
        )

    def event(self, event):
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                self.engine.game_state = self.engine.game_state.TITLE_SCREEN
            elif event.key == pygame.K_q:
                self.engine.game_state = self.engine.game_state.TITLE_SCREEN
            elif event.key == pygame.K_w:
                self.key_manager.key_down('W')
            elif event.key == pygame.K_a:
                self.key_manager.key_down('A')
            elif event.key == pygame.K_s:
                self.key_manager.key_down('S')
            elif event.key == pygame.K_d:
                self.key_manager.key_down('D')
            elif event.key == pygame.K_j:
                self.key_manager.key_down('J')
            elif event.key == pygame.K_k:
                self.key_manager.key_down('K')
            elif event.key == pygame.K_SPACE:
                self.key_manager.key_down('SPACE')

        if event.type == pygame.KEYUP:
            if event.key == pygame.K_w:
                self.key_manager.key_up('W')
            elif event.key == pygame.K_a:
                self.key_manager.key_up('A')
            elif event.key == pygame.K_s:
                self.key_manager.key_up('S')
            elif event.key == pygame.K_d:
                self.key_manager.key_up('D')
            elif event.key == pygame.K_j:
                self.key_manager.key_up('J')
            elif event.key == pygame.K_k:
                self.key_manager.key_up('K')
            elif event.key == pygame.K_SPACE:
                self.key_manager.key_up('SPACE')

    def update(self, dt):
        # set up actions
        last_key_down = self.key_manager.last_key_down()
        logging.debug(f'PlatformScreen last key down: {last_key_down}')
        if last_key_down == None:
            self.player.cmd_idle()
        elif last_key_down == 'W':
            self.player.cmd_jump()
        elif last_key_down == 'A':
            self.player.cmd_walk_left()
        elif last_key_down == 'S':
            self.player.cmd_crouch()
        elif last_key_down == 'D':
            self.player.cmd_walk_right()
        elif last_key_down == 'SPACE':
            self.player.cmd_jump()

        # update player
        self.player.update(dt)
        self.check_bounds(self.player)

        # update background image based on centerx position of character
        self.bg.update(self.player.rect.centerx)

        # update NPCs
        for townie in self.townies:
            townie.update(dt)
            self.check_bounds(townie)

    def check_bounds(self, entity):
        # entity.rect has:
        # x, y
        # top, left, bottom, right
        # topleft, bottomleft, topright, bottomright
        # midtop, midleft, midbottom, midright
        # center, centerx, centery
        bounce_x, bounce_y = 0, 0
        if entity.rect.bottom > self.ground_level:
            bounce_y = self.ground_level - entity.rect.bottom
        elif entity.rect.top < 0:
            bounce_y = - entity.rect.top
        elif entity.rect.left < 0:
            bounce_x = -entity.rect.left
        elif entity.rect.right > self.width:
            bounce_x = self.width - entity.rect.right
        if abs(bounce_x) > 0 or abs(bounce_y) > 0:
            entity.bounce(bounce_x, bounce_y)

    def render(self, screen):
        # render entities
        self.bg.render(screen)
        self.player.render(screen)
        for townie in self.townies:
            townie.render(screen)

        # render annotations
        screen.blit(self.instruction_text, (0, 0))
