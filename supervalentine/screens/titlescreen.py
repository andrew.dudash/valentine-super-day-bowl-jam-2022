import pygame


class TitleScreen:

    def __init__(self, engine, width, height):
        self.engine = engine
        self.width = width
        self.height = height

        self.title_font = pygame.font.Font(None, 36)
        self.title_color = (32, 32, 32)
        self.title_text = self.title_font.render(
            'Super Valentine',
            1,
            self.title_color
        )

    def event(self, event):
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                self.engine.game_state = self.engine.game_state.STOPPED
            elif event.key == pygame.K_q:
                self.engine.game_state = self.engine.game_state.STOPPED
            elif event.key == pygame.K_RETURN:
                self.engine.game_state = self.engine.game_state.PLAYING

    def update(self, dt):
        pass

    def render(self, screen):
        screen.blit(self.title_text, (0, 0))
