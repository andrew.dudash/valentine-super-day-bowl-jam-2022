import logging
import pygame

from enum import Enum, auto

from screens.platformscreen import PlatformScreen
from screens.titlescreen import TitleScreen

GAME_TITLE = 'Super Valentine'

SCREEN_WIDTH, SCREEN_HEIGHT = 1280, 800
DEFAULT_TICK_FREQUENCY = 60


class GameState(Enum):
    STOPPED = auto()
    TITLE_SCREEN = auto()
    PLAYING = auto()


class Engine():

    def __init__(self):
        self.screen_width = SCREEN_WIDTH
        self.screen_height = SCREEN_HEIGHT

        self.screen_list = []
        self.game_state = GameState.STOPPED

        # wait for pygame init
        self.title_screen = None
        self.platform_screen = None

    def push_game_screen(self, game_screen):
        """Push a new game screen. If a game screen already exists, the current game screen will be paused at the start of the next game loop iteration."""
        logging.debug('Adding game screen: %s', (game_screen))
        self.screen_list.append(game_screen)

    def pop_game_screen(self):
        """Pop the current game screen. The previous game screen will be resumed at the start of the next game loop iteration.
        If no game screen exists, the game will terminate at the start of the next game loop iteration."""
        old_game_screen = self.screen_list.pop()
        logging.debug('Popped game screen: %s', (old_game_screen))

    def run(self):
        pygame.init()

        self.title_screen = TitleScreen(
            self, self.screen_width, self.screen_height)
        self.platform_screen = PlatformScreen(
            self, self.screen_width, self.screen_height)

        screen = pygame.display.set_mode(
            (self.screen_width, self.screen_height))
        pygame.display.set_caption(GAME_TITLE)

        background = pygame.Surface(screen.get_size())
        background = background.convert()
        background.fill((255, 255, 255))

        screen.blit(background, (0, 0))
        pygame.display.flip()

        clock = pygame.time.Clock()
        dt = 0
        self.game_state = GameState.TITLE_SCREEN
        self.push_game_screen(self.title_screen)

        while self.game_state != GameState.STOPPED:
            previous_game_state = self.game_state
            current_screen = self.screen_list[-1]
            for event in pygame.event.get():
                # logging.debug('Recieved event: %s', (event))
                if event.type == pygame.QUIT:
                    self.game_state = GameState.STOPPED
                else:
                    current_screen.event(event)
            if self.game_state != previous_game_state:
                logging.debug(
                    f'Game state changed from {previous_game_state} to {self.game_state}.')
                if self.game_state == GameState.STOPPED:
                    break
                elif self.game_state == GameState.TITLE_SCREEN:
                    if previous_game_state == GameState.PLAYING:
                        self.pop_game_screen()
                elif self.game_state == GameState.PLAYING:
                    if previous_game_state == GameState.TITLE_SCREEN:
                        self.push_game_screen(self.platform_screen)
                    else:
                        # other screen on top of platform screen
                        self.pop_game_screen()
                current_screen = self.screen_list[-1]
            current_screen.update(dt)
            screen.blit(background, (0, 0))
            current_screen.render(screen)
            pygame.display.flip()
            dt = clock.tick(DEFAULT_TICK_FREQUENCY)

        pygame.quit()
