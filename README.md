# Super Valentine

A Super Mario-like platformer with a Saint Valentine theme and Smash Bros physics. Also some crazy bad guys and awesome graphics.

## How to run

### Install prerequisites

```
python3 -m pip install -r supervalentine/requirements.txt
```

### Launch the game

```
python3 supervalentine
```
