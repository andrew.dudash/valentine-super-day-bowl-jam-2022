import pygame
import itertools
import logging

WINDOW_TITLE = 'Test Jump'
DEFAULT_TICK_FREQUENCY = 5
SCREEN_WIDTH, SCREEN_HEIGHT = 64, 64

if __name__ == '__main__':
    logging.getLogger().setLevel(logging.INFO)
    pygame.init()
    SCREEN = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT,))
    pygame.display.set_caption(WINDOW_TITLE)

    # Create an Empty Image to Clear Screen With
    BACKGROUND = pygame.Surface(SCREEN.get_size())
    BACKGROUND = BACKGROUND.convert()
    BACKGROUND.fill((255, 255, 255))

    JUMP_1 = pygame.image.load('./valentine_jump_down_64x64_1.png')
    JUMP_2 = pygame.image.load('./valentine_jump_down_64x64_2.png')

    JUMP_CYCLE = itertools.cycle([
        JUMP_1,
        JUMP_2
    ])

    # Clear the Screen
    SCREEN.blit(BACKGROUND, (0, 0))
    pygame.display.flip()

    # Main Game Loop
    CLOCK = pygame.time.Clock()
    RUNNING = True
    while RUNNING:
        for EVENT in pygame.event.get():
            if EVENT.type == pygame.QUIT:
                RUNNING = False
        SCREEN.blit(BACKGROUND, (0, 0))
        SCREEN.blit(next(JUMP_CYCLE), (0, 0))
        pygame.display.flip()
        CLOCK.tick(DEFAULT_TICK_FREQUENCY)
    pygame.quit()
