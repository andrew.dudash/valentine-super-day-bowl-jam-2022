import pygame
import itertools
import logging

WINDOW_TITLE = 'Test Walk'
DEFAULT_TICK_FREQUENCY = 3
SCREEN_WIDTH, SCREEN_HEIGHT = 64, 64

if __name__ == '__main__':
    logging.getLogger().setLevel(logging.INFO)
    pygame.init()
    SCREEN = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT,))
    pygame.display.set_caption(WINDOW_TITLE)

    # Create an Empty Image to Clear Screen With
    BACKGROUND = pygame.Surface(SCREEN.get_size())
    BACKGROUND = BACKGROUND.convert()
    BACKGROUND.fill((255, 255, 255))

    WALK_1 = pygame.image.load('./valentine_walk_64x64_1.png')
    WALK_2 = pygame.image.load('./valentine_walk_64x64_2.png')
    WALK_3 = pygame.image.load('./valentine_walk_64x64_3.png')

    WALK_CYCLE = itertools.cycle([
        WALK_1,
        WALK_2,
        WALK_3,
        WALK_2
    ])

    # Clear the Screen
    SCREEN.blit(BACKGROUND, (0, 0))
    pygame.display.flip()

    # Main Game Loop
    CLOCK = pygame.time.Clock()
    RUNNING = True
    while RUNNING:
        for EVENT in pygame.event.get():
            if EVENT.type == pygame.QUIT:
                RUNNING = False
        SCREEN.blit(BACKGROUND, (0, 0))
        SCREEN.blit(next(WALK_CYCLE), (0, 0))
        pygame.display.flip()
        CLOCK.tick(DEFAULT_TICK_FREQUENCY)
    pygame.quit()
